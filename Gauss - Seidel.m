disp('Enter A like [16 2 3; 4 -9 3; -5 8 12]')
A=input('')
for ii=1:size(A,1)
   if abs(A(ii,ii)) <= abs( sum(A(ii,:))- A(ii,ii))
       error('A is not Diagonal matrix')
   end
end
disp('Enter B like [15 7 -9; 6 8 7; 5 -8 13]')
B=input('')
disp('Enter X like [0 0 0; 0 0 0 0]')
x=input('')
n=size(x,1);
normVal=Inf; 
%% 
% * _*Tolerence for method*_
tol=1e-5; itr=0;
%% Algorithm: Gauss Seidel Method
%%
while normVal>tol
    x_old=x;
    
    for i=1:n
        
        sigma=0;
        
        for j=1:i-1
                sigma=sigma+A(i,j)*x(j);
        end
        
        for j=i+1:n
                sigma=sigma+A(i,j)*x_old(j);
        end
        
        x(i)=(1/A(i,i))*(B(i)-sigma);
    end
    
    itr=itr+1;
    normVal=norm(x_old-x);
end
%%
fprintf('Solution of the system is : \n%f\n%f\n%f\n%f in %d iterations',x,itr);
