clc;
clear all;
f=input('');
a=input('Enter lower limit a: '); % exmple a=1
b=input('Enter upper limit b: ');  % exmple b=2
ET=input ('');
syms x
sf=split(func2str(f),"@(x)");
sf=str2sym(sf(2));
t2=diff(sf,2);
t2=matlabFunction(t2);
h1=abs(t2(a));
h2=abs(t2(b));
maxfzeg=max([h1 h2]);
main=(b-a)^2 *maxfzeg;
N=sqrt( main / (12*ET) );
h=(b-a)/N;
disp(['N > ',num2str(N)])
disp(['h= ',num2str(h)])
